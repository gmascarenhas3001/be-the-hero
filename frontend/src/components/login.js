import React, { useState } from 'react';
import { Link, useHistory  } from 'react-router-dom';

import 'typeface-roboto';
import { Grid, TextField, Button } from '@material-ui/core';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

import HeroImage from '../assets/heroes.png';
import Logo from '../assets/logo.svg';
import api from '../services/api';


export default function Login() {
    const [ id, setId ] = useState('');
    const history = useHistory();

    async function handleLogin(e) {
        e.preventDefault();

        try {
            const response = await api.post('session', { id });
            
            localStorage.setItem('ongId', id);
            localStorage.setItem('ongName', response.data.name);

            history.push('/profile');

        }catch (err) {
            alert('Login failed, try against!')
        }
    }
   
        return (
            <div>
               <Grid container
               justify="center"
               alignItems="center" >
                   <Grid item>
                       <Grid>
                         <img src={Logo} alt="logo"/>
                       </Grid>
                        <form onSubmit={handleLogin}>
                            <TextField 
                                id="filled-basic" 
                                fullWidth="45px" 
                                label="Votre ID"
                                variant="filled"
                                value={id}
                                onChange={e => setId(e.target.value)}
                             />
                            <Grid >
                                <Button type="submit" variant="contained" color="primary" disableElevation>
                                   Login
                                </Button>
                            </Grid>  
                            <Grid>
                                <Link to="/register">
                                    <ExitToAppIcon /> Je suis pas enregistré    
                                </Link>
                               
                            </Grid>                
                        </form>
                    </Grid>
               
                    <Grid item>
                    <img src={HeroImage} alt="heroes"/>
                    </Grid>
                

               </Grid>
                
                
            </div>
        )
    
}

