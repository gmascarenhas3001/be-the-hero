import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';

import api from '../services/api';

import Logo from '../assets/logo.svg';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { Grid, Button, Card } from '@material-ui/core';

export default function Register() {
    const history = useHistory();

    const [name, setName ] = useState('');
    const [email, setEmail ] = useState('');
    const [whatsapp, setWhatsapp ] = useState('');
    const [city, setCity ] = useState('');
    const [uf, setUf ] = useState('');

    
    async function handleSubmit(e) {
        e.preventDefault();

        const data = {
            name,
            email,
            whatsapp,
            city,
            uf
        }
       
    const response = await api.post('/ongs', data);
        try {
            alert(`Votre ID ${response.data.id}`);

            history.push('/');
        }catch (err) {
            console.log(err);
        }
   }

        return (
            <div>
                <h1>Register</h1>
                <Card>
                    <Grid container direction="row" alignItems="center">
                       
                            <Grid item xs={6} >
                                <Grid >
                                    <img src={Logo} alt="Logo" />
                                </Grid>      
                                <Grid>
                                    <h1>Enregistrer</h1>
                                   <p>Ici vous pouvez enregistrer votre ONG et recevez un ID</p>
                                </Grid>
                                <Grid>
                                    <ArrowBackIcon />
                                    <Link to="/">retour vers Login</Link>
                                </Grid>
                            </Grid>  
                            <Grid item xs={6}>
                            <form onSubmit={handleSubmit}>
                                    <div>
                                        <input 
                                            type="text" 
                                            placeholder="name"
                                            onChange={e => setName(e.target.value)} 
                                            value={name} 
                                        />                           
                                    </div>
                                    <div>                       
                                        <input 
                                            type="email" 
                                            placeholder="email" 
                                            onChange={e => setEmail(e.target.value)}
                                            value={email}    
                                    />                      
                                    </div>
                                    <div>
                                        <input 
                                            type="text"
                                            placeholder="whatsapp"
                                            onChange={e => setWhatsapp(e.target.value)} 
                                            value={whatsapp} 
                                        />
                                    </div>
                                    <div>
                                        <input 
                                            type="text"
                                            placeholder="city"
                                            onChange={e => setCity(e.target.value)}
                                            value={city}       
                                        />
                                    </div>
                                    <div>
                                        <input
                                            type="text"
                                            placeholder="UF"
                                            onChange={e => setUf(e.target.value)}                                          
                                            value={uf}     
                                        />
                                    </div>
                                    <div>
                                    <Button type="submit" variant="contained"  color="primary" disableElevation>
                                         Enregister
                                    </Button>
                                    </div>
                                </form>
                            </Grid>                                         
                        </Grid>
                    </Card>  
            </div>
        )
    
}
