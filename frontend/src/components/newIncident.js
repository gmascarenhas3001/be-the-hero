import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';

import { Grid, Button, Card } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Logo from '../assets/logo.svg';
import api from '../services/api';

export default function NewIncident() {
    const [ title, setTitle ] = useState('');
    const [ descriptions, setDescription ] = useState('');
    const [ value, setValue ] = useState('');
    const history = useHistory();

    const ongId = localStorage.getItem('ongId');

    async function handleSubmit(e) {
        e.preventDefault();

        const data = {
            title,
            descriptions,
            value
        }

        try {
            await api.post('incidents', data, {
                headers: {
                    Authorization: ongId,
                }
            });
            history.push('/profile');


        } catch (err) {
            alert('Error new register');
        }
        
    }

    return (
        <Card>
            <Grid container direction="row" alignItems="center">
            
                    <Grid item xs={6} >
                        <Grid >
                            <img src={Logo} alt="Logo" />
                        </Grid>      
                        <Grid>
                            <h1>Enregistrer</h1>
                        <p>Ici vous pouvez enregistrer votre ONG et recevez un ID</p>
                        </Grid>
                        <Grid>
                            <ArrowBackIcon />
                            <Link to="/">retour vers Login</Link>
                        </Grid>
                    </Grid>  
                    <Grid item xs={6}>
                    <form onSubmit={handleSubmit}>
                            <div>
                                <input 
                                    type="text" 
                                    placeholder="title"
                                    onChange={e => setTitle(e.target.value)} 
                                    value={title} 
                                />                           
                            </div>
                            <div>                       
                                <input 
                                    type="text" 
                                    placeholder="description" 
                                    onChange={e => setDescription(e.target.value)}
                                    value={descriptions}    
                            />                      
                            </div>
                            <div>
                                <input 
                                    type="text"
                                    placeholder="value"
                                    onChange={e => setValue(e.target.value)} 
                                    value={value} 
                                />
                            </div>
                        
                            <div>
                            <Button type="submit" variant="contained"  color="primary" disableElevation>
                                Enregister
                            </Button>
                            </div>
                        </form>
                    </Grid>                                         
                </Grid>
            </Card>  
        )
}