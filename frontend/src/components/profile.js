import React, { useEffect, useState } from 'react';
import { Grid, Button, Card } from '@material-ui/core';
import Logo from '../assets/logo.svg';
import { Link, useHistory } from 'react-router-dom';

import api from '../services/api';

export default function Profile() {
    const ongName = localStorage.getItem('ongName');
    const ong_id = localStorage.getItem('ongId');
    const history = useHistory();

    const [ incidents, setIncidents ] = useState([]);

    useEffect(() => {
        api.get('profile', {
            headers: {
                Authorization: ong_id
            }
        }).then(response => {
            setIncidents(response.data);
        })
    }, [ong_id]);

    async function handleDeleteIncident(id) {
        try {
            await api.delete(`incidents/${id}`, {
                headers: {
                    Authorization: ong_id
                }
            })

            setIncidents(incidents.filter(incident => incident.id !== id));
            
        } catch (err) {
            alert('Error not delete Incident, try again')
        }
    }

    function handleLogout() {
        localStorage.clear();
        history.push('/');
    }
   

    return (
        <div>
           <Grid container justify="space-between" direction="row" alignItems="center">
              <Grid item>
                <img src={Logo} alt="Logo"/>
                </Grid>
                <Grid item >
                 Beinvenu, {ongName}          
                </Grid>
              <Grid item>
                  <Link to="/incident/new">
                  <Button color="secondary"  variant="contained" disableElevation>register new incidents</Button>
                  </Link>
                  
                  <Button onClick={handleLogout}>Logout</Button>

              </Grid>
           </Grid>

           <Grid container direction="row" spacing={3}>
              
                
               {incidents.map(incident => (
                    <Grid item xs={6} key={incident.id}>
                     <Card>
                        <strong>Case</strong>
                            <p>{incident.title}</p>
                        <strong>Description</strong>
                            <p>{incident.descriptions}</p>
                        <strong>value</strong>
                           <p>{Intl.NumberFormat('pt-BR', {style: 'currency', currency: 'BRL'}).format(incident.value)}</p>
                        <Button type="submit" onClick={() => handleDeleteIncident(incident.id)} variant="contained" color="primary">Delete</Button>
                     </Card>
                     </Grid>
                ))}
                                      
              
           </Grid>

        </div>
    );


};