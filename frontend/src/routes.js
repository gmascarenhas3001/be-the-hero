import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Login from './components/login';
import Register from './components/register';
import Profile from './components/profile';
import NewIncident from './components/newIncident';




export default function Routes() {
    return( 
    <BrowserRouter>
        <Switch>
            <Route path='/' exact component={Login} />
            <Route path='/register' component={Register} />
            <Route path='/profile' component={Profile} />
            <Route path='/incident/new' component={NewIncident} />


        </Switch>
    </BrowserRouter>
    );
};