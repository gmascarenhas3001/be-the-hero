const express = require('express');
const cors = require('cors');
const PORT = 3001;
const routes = require('./routes');

const app = express();

app.use(cors());
app.use(express.json());
app.use(routes);

app.listen(PORT, function() {
    console.log(`App listening on port ${PORT}!`)
});

module.exports = app;